from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from accounts.forms import LoginForm, SignUpForm
from django.contrib.auth.models import User


def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            # if the person successfully logs in,
            if user is not None:
                login(request, user)
                # redirect them to home
                return redirect("home")
    else:
        # this is displaying the form when there is GET method
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            print(form.cleaned_data)

        if password == password_confirmation:
            # create user method
            user = User.objects.create_user(
                username,
                password=password,
            )
            # login function
            login(request, user)

            return redirect("home")

        else:
            # for.add_error means user is not created
            form.add_error("password", "the passwords do not match")
            # what is "password" up above?

    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
