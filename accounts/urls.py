from django.urls import path
from accounts.views import user_login, user_logout, signup

urlpatterns = [
    # first part is url path in browser
    # middle part is what you  named your view fxn
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
]
