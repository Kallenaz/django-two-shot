from django.urls import path
from receipts.views import (
    receipt_list,
    create_receipt,
    ExpenseCategory_list,
    Account_list,
    create_expenseCategory,
    create_account,
)

urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", ExpenseCategory_list, name="category_list"),
    path("accounts/", Account_list, name="account_list"),
    path("categories/create/", create_expenseCategory, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
